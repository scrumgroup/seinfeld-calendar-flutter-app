import 'package:aweasome/model/planModel.Dart';
import 'package:aweasome/service/planService.dart';
import 'package:aweasome/service/checkService.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:firebase_auth/firebase_auth.dart';

class MainService {

  void addEvent(String title, DateTime startDate, DateTime endDate) async {
    getUserId().then((uid) {
      if (startDate.isAfter(endDate) || endDate.isBefore(DateTime.now()))
        toastMessage("Unvalid Start and End Date! Re-enter Dates!");
      else {
        Plan plan = new Plan(
            title: title,
            userId: uid,
            startAt: startDate,
            endAt: endDate,
            status: false,
            createAt: DateTime.now()
        );
        print(plan.userId);
        new PlanService().addPlan(plan)
            .then((docRef) {
          toastMessage("Add new Event Success");
        }
        ).catchError((err) => toastMessage(err.toString()));
      }
    });
  }

  void checkEvent(String checkId) async {
    Firestore.instance.collection("check").document(checkId).get().then((docRef) {
      var data = docRef.data['isFinished'] ==  true ? {"isFinished": false} : {"isFinished": true};
      print(data.toString());
      new CheckService().updateCheck(data, checkId)
          .then((docRef) => toastMessage("Update event status!!"))
          .catchError((err) => toastMessage(err.toString()));
    }).catchError((err) => print(err.toString()));
  }
  
  Future<double> getSuccess(DateTime date) async {
    double perc = 0;
    await Firestore.instance.collection("check").where("date", isEqualTo: date).getDocuments()
      .then((docRefs){
        docRefs.documents.forEach((doc){
          if(doc.data["isFinished"] == true)
            perc += 1;
          });
        perc /= docRefs.documents.length;
      }
      );
    return perc;
  }

  Future<double> getFailed(DateTime date) async {
    double perc = 0;
    if(DateTime.now().isAfter(date.add(new Duration(days: 1))))
      await Firestore.instance.collection("check").where("date", isEqualTo: date).getDocuments()
        .then((docRefs){
          docRefs.documents.forEach((doc){
            if(doc.data["isFinished"] == false)
              perc += 1;
            }
          );
          perc /= docRefs.documents.length;
        }
      );
    return perc;
  }
}

void toastMessage(String msg){
  Fluttertoast.showToast(
      msg: msg,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.CENTER,
      timeInSecForIos: 1,
      backgroundColor: Colors.blueAccent,
      textColor: Colors.white,
      fontSize: 16.0
  );
}

Future<String> getUserId() async {
  final user = await FirebaseAuth.instance.currentUser();
  return user.uid;
}
