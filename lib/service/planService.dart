
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:aweasome/model/planModel.Dart';
import 'package:aweasome/commun/callService.dart';
import 'package:flutter/cupertino.dart';
import 'package:aweasome/model/checkModel.Dart';
import 'package:aweasome/service/checkService.dart';

class PlanService extends ChangeNotifier{
   CallService _api;
   List<Plan> plans;

  PlanService(){
    _api = new CallService("plan");
    plans=new List<Plan>();
  }

  Future<List<Plan>> fetchPlans() async {
     await _api.getDB().getDocuments()
    .then((data) => data.documents.forEach((doc)=>plans.add(Plan.fromMap(doc.data, doc.documentID))));
    print(plans.length);
    return plans;
  }

  Stream<QuerySnapshot> fetchPlansAsStream() {
    return _api.streamDataCollection();
  }

  Future<Plan> getPlanById(String id) async {
    var doc = await _api.getDocumentById(id);
    return  Plan.fromMap(doc.data, doc.documentID) ;
  }


  Future removePlan(String id) async{
     await _api.removeDocument(id) ;
     return ;
  }
  Future updatePlan(Map data,String id) async{
    await _api.updateDocument(data, id) ;
    return ;
  }

  Future addPlan(Plan data) async{
    var result  = await _api.addDocument(data.toJson())
      .then((docRef){
          var duration = data.endAt.difference(data.startAt).inDays;
          print(duration);
          for (int i=0; i < duration; i++){
            var date = data.startAt.add(new Duration(days: i));
            var check = new CheckService().addCheck(
              new Check(
                userID: data.userId,
                planID: docRef.documentID.toString(),
                date: date,
                isFinished: false,
                createAt: data.createAt
              )
            );
            check.then((checkId)=> docRef.updateData({"members": FieldValue.arrayUnion([checkId])}));
          }
        }
      );
    return result;
  }

}