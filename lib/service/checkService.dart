
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:aweasome/model/checkModel.Dart';
import 'package:aweasome/commun/callService.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:aweasome/service/planService.dart';
import 'package:aweasome/model/planModel.Dart';

class CheckService {
  CallService _api;
  List<Check> checks;
  PlanService serviceplan;
  CheckService(){
    _api = new CallService("check");
  }
  Future<List<Check>> fetchChecks() async {
    var result = await _api.getDataCollection();
    checks = result.documents
        .map((doc) =>  Check.fromMap(doc.data,doc.documentID)).toList();
    return checks;
  }
Future<List<String>> fetchChecks_byDate(DateTime date, String userID) async {
     serviceplan = new PlanService();
     List<String> listWork= new List<String>();
    var result = await _api.getDB()
                .where('date', isEqualTo: date)
                .where('userId', isEqualTo: userID)
                .where('isFinished', isEqualTo: false).getDocuments();
    // var result = await _api.getDB()
    //             .where('userId', isEqualTo: userID).getDocuments();
    checks = result.documents
        .map((doc) =>  Check.fromMap(doc.data,doc.documentID)).toList();
    if(checks.length > 0){
      for(var item in checks){
         await serviceplan.getPlanById(item.planID).then((onValue)=>listWork.add(onValue.title));
      }
    }
    print(listWork);
    return listWork;
}
  Stream<QuerySnapshot> fetchChecksAsStream() {
    return _api.streamDataCollection();
  }

  Future<Check> getCheckById(String id) async {
    var doc = await _api.getDocumentById(id);
    return  Check.fromMap(doc.data, doc.documentID) ;
  }

  Future removeCheck(String id) async{
    await _api.removeDocument(id) ;
    return ;
  }
  Future updateCheck(Map data,String id) async{
    await _api.updateDocument(data, id) ;
    return ;
  }

  Future addCheck(Check data) async{
    var result  = await _api.addDocument(data.toJson()).catchError((err) => print(err));
    return result.documentID ;
  }
}