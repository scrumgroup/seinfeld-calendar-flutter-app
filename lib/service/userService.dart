import 'package:firebase_auth/firebase_auth.dart';
class UserService{
  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<FirebaseUser> getUser() async {
    return await _auth.currentUser();
  }
  Future<String> getUserId() async {
  final user = await FirebaseAuth.instance.currentUser();
  return user.uid;
}
}