import 'package:flutter/material.dart';
import 'package:aweasome/src/screens/login_page/login_page.dart';
import 'package:aweasome/src/screens/home/home_page.dart';
import 'package:aweasome/src/screens/calendarMonth/month_Calendar.dart';
import 'package:aweasome/src/screens/calendarYear/year_Calendar.dart';
import 'package:aweasome/src/screens/signup_page/signup_page.dart';
import 'package:aweasome/src/screens/DailyCalendar/daily_calendar.dart';

class Router {
  static final String homeRoute = '/';
  static final String monthCalendarRoute = "/monthCalendar";
  static final String yearCalendarRoute = "/yearCalendar";
  static final String loginRoute = "/login";
  static final String signupRoute = "/signup";
  static final String dailyCalendarRoute = "/dailyCalendar";
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/login':
        return MaterialPageRoute(builder: (_) => LoginPage());
      case '/':
        return MaterialPageRoute(builder: (_) => HomePage());
      case '/signup':
        return MaterialPageRoute(builder: (_) => SignupPage());
      case '/monthCalendar':
        var data = settings.arguments as String;
        return MaterialPageRoute(builder: (_) => MonthCalendarPage(data));
      case '/yearCalendar':
        return MaterialPageRoute(builder: (_) => YearCalendarPage());

      default:
        return MaterialPageRoute(
            builder: (_) => Scaffold(
                  body: Center(
                      child: Text('No route defined for ${settings.name}')),
                ));
    }
  }
}
