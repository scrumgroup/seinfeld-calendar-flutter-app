import 'package:aweasome/service/checkService.dart';
import 'package:aweasome/src/screens/DailyCalendar/daily_calendar.dart';
import 'package:aweasome/src/screens/calendarYear/year_Calendar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart'
    show CalendarCarousel;
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_calendar_carousel/classes/event_list.dart';
import 'package:intl/intl.dart' show DateFormat;
import 'package:aweasome/service/planService.dart';
import 'package:aweasome/model/planModel.Dart';
import 'package:aweasome/model/checkModel.Dart';
import 'package:aweasome/src/routes/router.dart';
import 'package:aweasome/src/screens/DailyCalendar/daily_calendar.dart';
class MonthCalendarPage extends StatelessWidget {
  // This widget is the root of your application.
  MonthCalendarPage(this.data);
  final String data;
  List<String> time;
  @override
  Widget build(BuildContext context) {
    print("truyenthamso $data");
    time=data.split("/");
    return new MaterialApp(
      title: 'dooboolab flutter calendar',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MonthCalendarItem(title: 'Flutter Calendar Carousel Example',cur_month: int.tryParse(time[0]),cur_year: int.tryParse(time[1])),
    );
  }
}

class MonthCalendarItem extends StatefulWidget {
  MonthCalendarItem({Key key, this.title, this.cur_year,this.cur_month}) : super(key: key);
  final String title;
  final int cur_month;
  final int cur_year;
  @override
  _MonthCalendarItem createState() => new _MonthCalendarItem(transac_month : this.cur_month, transac_year: this.cur_year);
}

class _MonthCalendarItem extends State<MonthCalendarItem> {
  //DateTime _currentDate2 = DateTime(2019, 2, 3);

 _MonthCalendarItem({this.transac_month,this.transac_year});
  int  transac_month;
  int transac_year;
  String _currentMonth = '';
  DateTime _currentDate2;
  CheckService checkAPI = new CheckService();
  static Widget _finishIcon = new Container(
    decoration: new BoxDecoration( 
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(1000)),
        border: Border.all(color: Colors.blue, width: 2.0)),
    child: new Icon(
      Icons.account_box,
      color: Colors.black,
    ),
  );
   
  static Widget _unfinshIcon = new Container(
    decoration: new BoxDecoration( 
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(1000)),
        border: Border.all(color: Colors.red, width: 2.0)),
    child: new Icon(
      Icons.access_time,
      color: Colors.brown,
    ),
 
  );
  EventList<Event> _markedDateMap = new EventList<Event>();
  CalendarCarousel _calendarCarousel;
  List<Plan> listPlan;
  List<Check> listCheck;
  EventList<Event> initEvenList(List<Check> checks){
    EventList<Event> temp =new EventList<Event>();
    print(checks);
    checks.forEach((ele){
          if(ele.isFinished)  
             temp.add(ele.date,
                              new Event(date: ele.date,
                                      title: 'Event',
                                    icon: _finishIcon));
          else 
            temp.add(ele.date,
                              new Event(date: ele.date,
                                      title: 'Event',
                                      icon: _unfinshIcon));
    });          
     return temp;
  }
  @override
  void initState() { 
     checkAPI.fetchChecks().then((value){
       listCheck=value;
       _markedDateMap=initEvenList(listCheck);
       })
                           .catchError((err) => print(err));
      
     _currentDate2=new DateTime(transac_year,transac_month,1);  
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    _calendarCarousel = CalendarCarousel<Event>(
      todayBorderColor: Colors.green,
      onDayPressed: (DateTime date, List<Event> events) {
        this.setState(() => _currentDate2 = date);
        Navigator.push(context, MaterialPageRoute(builder: (context) => DailyPage(date: _currentDate2)));
      },
      daysHaveCircularBorder: true,
      showOnlyCurrentMonthDate: false,
      weekendTextStyle: TextStyle(
        color: Colors.red,
      ),
      thisMonthDayBorderColor: Colors.grey,
      weekFormat: false,
      firstDayOfWeek: 2,
      markedDatesMap: _markedDateMap,
      height: 600.0,
      selectedDateTime: _currentDate2,
      customGridViewPhysics: NeverScrollableScrollPhysics(),
      markedDateCustomShapeBorder: CircleBorder(
        side: BorderSide(color: Colors.yellow)
      ),
      markedDateCustomTextStyle: TextStyle(
        fontSize: 18,
        color: Colors.blue,
      ),
       markedDateShowIcon: true,
      markedDateIconBuilder: (event) {
        return event.icon;
      },
      markedDateIconMargin: 45,
      markedDateIconOffset: 0,
      todayTextStyle: TextStyle(
        color: Colors.blue,
      ),
      todayButtonColor: Colors.yellow,
      selectedDayTextStyle: TextStyle(
        color: Colors.yellow,
      ),
      minSelectedDate: _currentDate2.subtract(Duration(days: 360)),
      maxSelectedDate: _currentDate2.add(Duration(days: 360)),
      prevDaysTextStyle: TextStyle(
        fontSize: 16,
        color: Colors.pinkAccent,
      ),
      inactiveDaysTextStyle: TextStyle(
        color: Colors.tealAccent,
        fontSize: 16,
      ),
      onCalendarChanged: (DateTime date) {
        this.setState(() => _currentMonth = DateFormat.yMMM().format(date));
      },
      onDayLongPressed: (DateTime date) {
        print('long pressed date $date');
        //Navigator.push(context, MaterialPageRoute(builder: (context) => DailyPage(date: _currentDate2)));
        Navigator.of(context).push(new MaterialPageRoute(builder: (_)=>DailyPage(date: _currentDate2)),)
        .then((val)=>val? initState():null);
      },
    );

    return new Scaffold(
        appBar: new AppBar(
          leading: IconButton(icon: Icon(Icons.arrow_back), tooltip:"Year Calendar", onPressed: () =>Navigator.push(context, new MaterialPageRoute( builder: (context) => new YearCalendarPage()))),
          title: Center(child: Text('Month Calendar'),),
        ),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.symmetric(horizontal: 16.0),
                child: _calendarCarousel,
              ),//
            ],
          ),
        ));
  }
}