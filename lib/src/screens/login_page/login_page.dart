import 'package:aweasome/service/userService.dart';
import 'package:aweasome/src/screens/calendarYear/year_Calendar.dart';
import 'package:aweasome/src/screens/signup_page/signup_page.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:aweasome/src/routes/router.dart';
import 'package:shared_preferences/shared_preferences.dart';
class LoginPage extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool isHidden = true;
  UserService user;
  TextEditingController userController = new TextEditingController();
  TextEditingController passController = new TextEditingController();
  bool isCorrectUsername = true;
  bool isCorrectPassword = true;
  var userError = 'Invalid Email';
  var passError = 
  '''Should contain at least 8 characters include lower case''';
  @override
  void initState(){
    //  user = new UserService();
    //  user.getUser().then((value){
    //      if(value != null){
    //         Navigator.pushNamed(context, Router.yearCalendarRoute);
    //      }
    //  }).catchError((err) => print(err));
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),  //max width, max height container
        color: Colors.white,
        padding: EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment:  MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 50, 0, 40),
                child: Container(
                  width: 70,
                  height: 70,
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.grey
                  ),
                  child: FlutterLogo()
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 40),
                child: Text(
                  "SEINFELD",
                  style: TextStyle(color: Colors.orange, fontSize: 30, fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                child: TextField(
                  controller: userController,
                  style: TextStyle(color: Colors.black, fontSize: 18),
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    errorText: isCorrectUsername? null : userError,
                    labelText: "Email",
                    labelStyle: TextStyle(fontSize: 15, color: Color(0xff888888))
                  ),
                ),
              ),
              Stack(
                alignment: AlignmentDirectional.centerEnd,
                children:<Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                    child: TextField(
                      controller: passController,
                      style: TextStyle(color: Colors.black, fontSize: 18),
                      obscureText: isHidden,
                      decoration: InputDecoration(
                        errorText: isCorrectPassword? null : passError,
                        labelText: "Password",
                        labelStyle: TextStyle(fontSize: 15, color: Color(0xff888888))
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: showPassword,
                    child: Text(
                      isHidden ? 'SHOW': 'HIDE',
                      style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.blue),                    
                    ),
                  )
                ]
              ),              
              SizedBox(
                width: double.infinity,
                height: 40,               
                child: RaisedButton(
                  color: Colors.blue,
                  onPressed: onSignInPress,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))
                  ),
                  child: Text(
                    'SIGN IN',
                    style: TextStyle(fontSize: 15)
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 40, 0, 40),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    GestureDetector(
                      onTap: ()=> Navigator.push(context, new MaterialPageRoute( builder: (context) => new SignupPage())),
                      child: Text(                                           
                        'Sign up',                                          
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      )
    );
  }
  void showPassword(){
    setState(() {
     isHidden = !isHidden; 
    });
  }
  void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Login Fail"),
          content: new Text("Incorrect Email or Password"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
  void onSignInPress(){
    setState(() {
     isValidEmail(userController.text.toString())? isCorrectUsername = true:isCorrectUsername = false;
     isValidPassword(passController.text.toString())? isCorrectPassword = true:isCorrectPassword = false;
     if(isCorrectPassword && isCorrectUsername){
       FirebaseAuth.instance.signInWithEmailAndPassword(email: userController.text, password: passController.text)
       .then((currentUser) => {
         addUserId(currentUser.user.uid),
         Navigator.push(context, new MaterialPageRoute( builder: (context) => new YearCalendarPage())),       
         })
       //Navigator.push(context, MaterialPageRoute(builder: (context) => HomePage()));
       .catchError((err) => _showDialog());
     }
    });
  }
  bool isValidEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return false;
    else
      return true;
  }

  bool isValidPassword(String value) {
    Pattern pattern =
        r'^(?=.*?[a-z]).{8,}$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return false;
    else
      return true;
  }

  addUserId(uid) async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt('uid', uid);
  }
}
