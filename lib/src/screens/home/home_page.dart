import 'package:aweasome/service/userService.dart';
import 'package:aweasome/src/screens/calendarYear/year_Calendar.dart';
import 'package:aweasome/src/screens/login_page/login_page.dart';
import 'package:flutter/material.dart';
import 'package:aweasome/src/routes/router.dart';
class HomePage extends StatefulWidget {
  HomePage() : super();
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  //
   bool isLogin = false;
  UserService user;
   @override
  void initState(){
     user = new UserService();
     user.getUser().then((value){
         if(value != null){
           isLogin=true;
            Navigator.pushNamed(context, Router.yearCalendarRoute);
         }
     }).catchError((err) => print(err));
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      color: Colors.white,
      child: Padding(
          padding: const EdgeInsets.all(30),
          child: Column(
            children: <Widget>[
              Expanded(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Welcome to FTEAM',
                    style: TextStyle(
                        fontSize: 30,
                        color: Colors.black
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20),
                  ),
                  Container(
                    width: 200,
                    height: 200,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            image: AssetImage('assets/images/doremon.jpg'),
                            fit: BoxFit.cover)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20),
                  ),
                  Text(
                    'Powerfull, intuitive, beautifull',
                    style: TextStyle(
                        fontSize: 20, color: Theme.of(context).primaryColor),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                  ),
                  Text(
                    'A hightly customizable, planner with multiple, calendar view, widgets for quick and easy scheduling.',
                    style: TextStyle(
                      fontSize: 14, color: Colors.black87
                    ),
                    textAlign: TextAlign.center,
                  )
                ],
              )),
              SizedBox(
                height: 20,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  FlatButton(
                    child: Text('Getting Started'),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)),
                    color: Theme.of(context).primaryColor,
                    textColor: Colors.white,
                    onPressed: () => isLogin ?  Navigator.push(context, new MaterialPageRoute( builder: (context) => new YearCalendarPage())) : Navigator.push(context, new MaterialPageRoute( builder: (context) => new LoginPage()))
                  )
                ],
              )
            ],
          )),
    ));
  }
}
