import 'package:aweasome/src/routes/router.dart';
import 'package:aweasome/src/screens/calendarMonth/month_Calendar.dart';
import 'package:aweasome/src/screens/home/home_page.dart';
import 'package:flutter/material.dart';
import 'package:aweasome/component/scrolling_years_calendar.dart';

class YearCalendarPage extends StatefulWidget {
  @override
  _YearCalendarState createState() => _YearCalendarState();
}
class _YearCalendarState extends State<YearCalendarPage> {
  Icon actionIcon = new Icon(Icons.search);
  Widget appBarTitle = new Text('');
  List<DateTime> getHighlightedDates() {
    List<DateTime> now =new List<DateTime>();
    now.add(DateTime.now());
    return now;
  }

  @override
  Widget build(BuildContext context) {
 
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue[400],
        leading: IconButton(icon: Icon(Icons.arrow_back), tooltip:"Home", onPressed: () => Navigator.push(context, new MaterialPageRoute( builder:(context) => new HomePage()))),
        title: Center(child: Text('Year Calendar'),)      
      ),
      body: Center(
        child: ScrollingYearsCalendar(
          // Required properties
          context: context,
          initialDate: DateTime.now(),
          firstDate: DateTime.now().subtract(const Duration(days: 8 * 365)),
          lastDate: DateTime.now(),
          currentDateColor: Colors.blue,

          // Optional properties
          highlightedDates: getHighlightedDates(),
          highlightedDateColor: Colors.deepOrange,
          monthNames: const <String>[
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec',
          ],
          onMonthTap: (int year, int month){
            Navigator.push(context, new MaterialPageRoute( builder: (context) => new MonthCalendarPage("$month/$year")));
           // Navigator.pushNamed(context,Router.monthCalendarRoute,arguments:"$month/$year");
            print('Tapped $month/$year');
          }
        ),
      ),
    );
  }
}