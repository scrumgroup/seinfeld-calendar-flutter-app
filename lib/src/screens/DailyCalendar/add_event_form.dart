import 'package:flutter/material.dart';
import 'package:aweasome/service/mainService.dart';

class AddEventForm extends StatefulWidget {
  @override
  _AddEventState createState() => _AddEventState();
}


class _AddEventState extends State<AddEventForm> {
  var service = new MainService();
  final title = TextEditingController();
  var startDate;
  var endDate;

  inputStartDate(BuildContext context) async {
    DateTime result = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2018),
      lastDate: DateTime(2030),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.dark(),
          child: child,
        );
      },
    );
    if(result != null) setState(() => startDate = result);
  }

  inputEndDate(BuildContext context) async {
    DateTime result = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2018),
      lastDate: DateTime(2030),
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.dark(),
          child: child,
        );
      },
    );
    if(result != null) setState(() => endDate = result);
  }

    @override
    Widget build(BuildContext context) {
      return Container(
        width: 1000,
        height: 310,
        child: ListView(
          children: <Widget>[
            Align(
              alignment: Alignment.centerLeft,
              child: Text('Title :',style: TextStyle(fontWeight: FontWeight.bold),),
              ),
            TextField(
              controller: title,
              decoration: InputDecoration(
                hintText: 'Enter event title'
              ),
            ),
            ButtonBar(children: <Widget>[
              Icon(Icons.date_range),
              RaisedButton(
                onPressed: (){
                  inputStartDate(context);
                },
                child: Text(startDate==null?'Select start time':startDate.toString()),
              ),
            ],),
            ButtonBar(children: <Widget>[
              Icon(Icons.date_range),
              RaisedButton(
                onPressed: (){
                  inputEndDate(context);
                },
                child: Text(endDate==null?'Select end time':endDate.toString()),
              ),
            ],),
            ButtonBar(
              children: <Widget>[
                RaisedButton(color: Colors.blue, onPressed: () => service.addEvent(title.text, startDate, endDate),child: Text("Submit"),),
              ],
            ),
          ],
        ),
      );
    }
}