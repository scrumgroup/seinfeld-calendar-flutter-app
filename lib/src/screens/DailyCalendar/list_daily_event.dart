import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:aweasome/service/mainService.dart';

class ListDailyEvents extends StatelessWidget {
  final userId = getUserId();
  final DateTime date;
  ListDailyEvents({Key key, @required this.date}) : super(key: key);
  @override
  Widget build(BuildContext context){
      print(DateTime.now());
      print(date);
      final parentHeight = MediaQuery.of(context).size.height;
      final parentWidth = MediaQuery.of(context).size.width;
      return FutureBuilder(
        future: userId,
        builder: (context, uid){
          Stream<QuerySnapshot> listEvent = Firestore.instance.collection("check")
              .where('date', isEqualTo: date)
              .where('userId', isEqualTo: uid.data).snapshots();
          return StreamBuilder(
            stream: listEvent,
            builder: (context, checks) {
              if (checks.hasData) {
                return ListView.separated(
                    padding: const EdgeInsets.all(8),
                    itemCount: checks.data.documents.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        decoration: const BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.all(
                              const Radius.circular(90.0)),
                        ),
                        height: 50,
                        child: Center(
                          child: Row(
                            children: <Widget>[
                              Container(
                                decoration: const BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: Colors.white,
                                ),
                                width: parentWidth * 0.1,
                                height: parentHeight,
                                padding: const EdgeInsets.all(4.0),
                                margin: const EdgeInsets.all(6.0),
                                child: Center(child: Text(index.toString(),
                                  style: TextStyle(fontWeight: FontWeight.bold),)),
                              ),
                              Container(
                                decoration:
                                  checks.data.documents[index].data['isFinished'].toString() == "true" ?
                                    const BoxDecoration(
                                      color:  Colors.greenAccent,
                                      borderRadius: BorderRadius.all(
                                          const Radius.circular(90.0)),
                                      )
                                  : DateTime.now().isAfter(date.add(new Duration(days: 1))) ?
                                    const BoxDecoration(
                                      color:  Colors.red,
                                      borderRadius: BorderRadius.all(
                                          const Radius.circular(90.0)),
                                      )
                                  : const BoxDecoration(
                                      color:  Colors.white,
                                      borderRadius: BorderRadius.all(
                                        const Radius.circular(90.0)),
                                      ),
                                width: parentWidth * 0.68,
                                height: parentHeight,
                                padding: const EdgeInsets.all(4.0),
                                margin: const EdgeInsets.all(6.0),
                                child: Center(
                                  child: FutureBuilder(
                                    future: Firestore.instance.collection("plan").document(checks.data.documents[index].data['planId']).get(),
                                    builder: (context, plan){
                                      if(plan.hasData)
                                        return Text(plan.data['title']);
                                      else
                                        return Text("");
                                    },
                                  ),
                                ),
                              ),
                              IconButton(
                                icon: Icon(Icons.edit, color: Colors.white,),
                                tooltip: 'Check this task',
                                color: Colors.white,
                                onPressed: () => DateTime.now().isAfter(date) ?
                                new MainService().checkEvent(checks.data.documents[index].documentID):toastMessage("Cannot check Event in Future!!!"),
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                    separatorBuilder: (BuildContext context, int index) => const Divider()
                );
              } else return Text("");
            },
          );
        }
      );
  }
}