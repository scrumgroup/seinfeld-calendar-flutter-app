import 'package:aweasome/service/mainService.dart';
import 'package:aweasome/src/routes/router.dart';
import 'package:flutter/material.dart';
import 'package:aweasome/src/screens/DailyCalendar/list_daily_event.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:aweasome/src/screens/DailyCalendar/add_event_form.dart';
import 'package:intl/intl.dart';

class DailyPage extends StatelessWidget {

  createTaskDialog(BuildContext context){
    return showDialog(context: context,builder: (context){
      return AlertDialog(
        title: Text('Add new event'),
        content: Container(
          child: AddEventForm()
        )
      );
    });
  }

  final DateTime date;
  DailyPage({Key key, @required this.date}) : super(key: key);

  @override
  Widget build(BuildContext context){
    final pageHeight = MediaQuery.of(context).size.height;
    final pageWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(icon: Icon(Icons.arrow_back),tooltip: "Month Calendar", onPressed: () => Navigator.pop(context,true),),
        title: Center(child: Text('Your daily detail'),),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.account_circle),tooltip: "Navigate to profile page", onPressed: null,),
        ],
      ),
      body: Center(
        child: Column(
          children: <Widget>[
            Text(date.day.toString(),style: TextStyle(
              color: Colors.green,
              fontSize: 100,
            ),),
            Text(DateFormat.yMMMMEEEEd().format(date),style: TextStyle(
              fontSize: 30,
              fontFamily: 'Pacifico',
            ),),
            Row(
              children: <Widget>[
                Container(
                  width: pageWidth * 0.5,
                  height: pageHeight*0.25,
                  child: Center(
                    child: FutureBuilder(
                      future: new MainService().getSuccess(date),
                      builder: (context, percent){
                        return CircularPercentIndicator(
                          radius: 120.0,
                          lineWidth: 13.0,
                          animation: true,
                          percent: percent.data ?? '0',
                          center: new Text(
                            (percent.data*100).toStringAsFixed(2) + "%",
                            style:
                            new TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
                          ),
                          footer: new Text(
                            "Success",
                            style:
                            new TextStyle(fontWeight: FontWeight.bold, fontSize: 17.0),
                          ),
                          circularStrokeCap: CircularStrokeCap.round,
                          progressColor: Colors.green,
                        );
                      }
                    ),
                  )
                ),
                Container(
                  width: pageWidth * 0.5,
                  height: pageHeight*0.25,
                  child: Center(
                    child: FutureBuilder(
                      future: new MainService().getFailed(date),
                      builder: (context, percent){
                        return CircularPercentIndicator(
                          radius: 120.0,
                          lineWidth: 13.0,
                          animation: true,
                          percent: percent.data,
                          center: new Text(
                            (percent.data*100).toString() + "%",
                            style:
                            new TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
                          ),
                          footer: new Text(
                            "Failed",
                            style:
                            new TextStyle(fontWeight: FontWeight.bold, fontSize: 17.0),
                          ),
                          circularStrokeCap: CircularStrokeCap.round,
                          progressColor: Colors.red,
                        );
                      },
                    )
                  ),
                )
              ],
            ),
            Container(
              height: pageHeight*0.35,
              child: ListDailyEvents(date: date),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: "Add event",
        child: Icon(Icons.add),
        onPressed: (){createTaskDialog(context);},
      ),
    );
  }
}