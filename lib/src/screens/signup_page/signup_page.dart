import 'package:flutter/material.dart';
import 'package:aweasome/src/routes/router.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:aweasome/src/routes/router.dart';

class SignupPage extends StatefulWidget {
  // This widget is the root of your application.
  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  UserUpdateInfo info = new UserUpdateInfo();
  bool isHidden = true;
  TextEditingController nameController = new TextEditingController();
  TextEditingController userController = new TextEditingController();
  TextEditingController passController = new TextEditingController();
  bool isEmptyName = false;
  bool isCorrectUsername = true;
  bool isCorrectPassword = true;
  var nameError = 'This field is required';
  var userError = 'Invalid Email';
  var passError = 
  '''Should contain at least 8 characters include lower case''';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),  //max width, max height container
        color: Colors.white,
        padding: EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment:  MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 50, 0, 40),
                child: Container(
                  width: 70,
                  height: 70,
                  padding: EdgeInsets.all(15),
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.grey
                  ),
                  child: FlutterLogo()
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 40),
                child: Text(
                  "SEINFELD",
                  style: TextStyle(color: Colors.orange, fontSize: 30, fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                child: TextField(
                  controller: nameController,
                  style: TextStyle(color: Colors.black, fontSize: 18),
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    errorText: isEmptyName ? nameError: null,
                    labelText: "Name",
                    labelStyle: TextStyle(fontSize: 15, color: Color(0xff888888))
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                child: TextField(
                  controller: userController,
                  style: TextStyle(color: Colors.black, fontSize: 18),
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    errorText: isCorrectUsername? null : userError,
                    labelText: "Email",
                    labelStyle: TextStyle(fontSize: 15, color: Color(0xff888888))
                  ),
                ),
              ),
              Stack(
                alignment: AlignmentDirectional.centerEnd,
                children:<Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
                    child: TextField(
                      controller: passController,
                      style: TextStyle(color: Colors.black, fontSize: 18),
                      obscureText: isHidden,
                      decoration: InputDecoration(
                        errorText: isCorrectPassword? null : passError,
                        labelText: "Password",
                        labelStyle: TextStyle(fontSize: 15, color: Color(0xff888888))
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: showPassword,
                    child: Text(
                      isHidden ? 'SHOW': 'HIDE',
                      style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold, color: Colors.blue),                    
                    ),
                  )
                ]
              ),              
              SizedBox(
                width: double.infinity,
                height: 40,               
                child: RaisedButton(
                  color: Colors.blue,
                  onPressed: onSignupPress,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(5))
                  ),
                  child: Text(
                    'SIGN UP',
                    style: TextStyle(fontSize: 15)
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 40, 0, 40),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    GestureDetector(
                      onTap: ()=> Navigator.pushNamed(context, Router.loginRoute),
                      child: Text(                                           
                        'Back to Login',                                          
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      )
    );
  }
  void showPassword(){
    setState(() {
     isHidden = !isHidden; 
    });
  }
  void _showFailedDialog(String err) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Signup Fail"),
          content: new Text(err),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
  void _showSuccessDialog(String success) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Signup Success"),
          content: new Text(success),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Login Now"),
              onPressed: () {
                Navigator.pushNamed(context,Router.loginRoute);
              },
            ),
            new FlatButton(
              child: new Text("Close"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }
  void onSignupPress(){
    setState(() {
      nameController.text.isEmpty ? isEmptyName = true: isEmptyName = false;
      isValidEmail(userController.text.toString())? isCorrectUsername = true:isCorrectUsername = false;
      isValidPassword(passController.text.toString())? isCorrectPassword = true:isCorrectPassword = false;
      if(isCorrectPassword && isCorrectUsername && !isEmptyName){
        FirebaseAuth.instance.createUserWithEmailAndPassword(email:userController.text, password: passController.text)
        .then((currentUser) => {
          info.displayName = nameController.text,
          currentUser.user.updateProfile(info)
          .then((onValue)=> _showSuccessDialog("Sign up Sucess"))
          .catchError((err)=>_showFailedDialog(err))
        })
        .catchError((err) => _showFailedDialog(err));     
     }
    });
  }
  bool isValidEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return false;
    else
      return true;
  }

  bool isValidPassword(String value) {
    Pattern pattern =
        r'^(?=.*?[a-z]).{8,}$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return false;
    else
      return true;
  }
}
