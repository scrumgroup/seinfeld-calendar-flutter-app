import 'package:aweasome/commun/utils/push_notification_helper.dart' as module;
import 'package:aweasome/service/checkService.dart';
import 'package:aweasome/service/mainService.dart';
import 'package:aweasome/service/userService.dart';
import 'package:flutter/material.dart';
import 'package:aweasome/src/routes/router.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter/cupertino.dart';
import 'package:rxdart/subjects.dart';
import 'package:android_alarm_manager/android_alarm_manager.dart';
void printHello () async{
  List<String> todoWork_Today=new List<String>();
  String userID;
  UserService userStore = new UserService();
  CheckService checkApi = new CheckService();
  module.LocalNotification pushNotafiction = module.LocalNotification.getNotiInstance();
  await userStore.getUserId().then((value) {
     print(value);
     userID = value;
  });
  await checkApi.fetchChecks_byDate(DateTime.now(),userID).then((list){
    todoWork_Today= list;
  });
  final DateTime now = DateTime.now();
  await pushNotafiction.showGroupedNotifications(todoWork_Today);
  print("[$now] Hello, world!");
}
Future<void> main() async {
  var now = new DateTime.now();
  var timeAlarm;
  if(now.hour>10) timeAlarm = now.add(new Duration(hours: 24-(now.hour-10)));
  else timeAlarm = now.add(new Duration(hours: 10-now.hour));
  final int helloAlarmID = 0;
  await AndroidAlarmManager.initialize();
  await AndroidAlarmManager.periodic(const Duration(minutes: 1), helloAlarmID, printHello, timeAlarm);
  runApp(
    new App()
  );
}
class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateRoute: Router.generateRoute,
      initialRoute: Router.homeRoute,
    );
  }
}

